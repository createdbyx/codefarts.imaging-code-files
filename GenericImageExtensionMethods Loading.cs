﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
namespace Codefarts.Imaging
{
    using System;
    using System.IO;

    /// <summary>
    /// Contains extension methods for the <see cref="GenericImage{T}"/> type.
    /// </summary>
    public static partial class GenericImageExtensionMethods
    {
        public static void ReadFromPPM(this GenericImage<Color> image, Stream stream)
        {
            var reader = new BinaryReader(stream);
            if (reader.ReadChar() != 'P' || reader.ReadChar() != '6')
            {
                return;
            }

            reader.ReadChar(); //Eat newline
            var widths = "";
            var heights = "";
            char temp;
            while ((temp = reader.ReadChar()) != ' ')
            {
                widths += temp;
            }

            while ((temp = reader.ReadChar()) >= '0' && temp <= '9')
            {
                heights += temp;
            }

            if (reader.ReadChar() != '2' || reader.ReadChar() != '5' || reader.ReadChar() != '5')
            {
                return;
            }

            reader.ReadChar(); // Eat the last newline
            int width = int.Parse(widths),
            height = int.Parse(heights);
            //var bitmap = new GenericImage<Color>(width, height);
            image.Width = width;
            image.Height = height;

            // Read in the pixels
            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    var Red = reader.ReadByte();
                    var Green = reader.ReadByte();
                    var Blue = reader.ReadByte();
                    image[x, y] = Color.FromArgb(255, Red, Green, Blue);
                }
            }

            // return bitmap;
        }


        /// <summary>
        /// Loads a <see cref="GenericImage{T}"/> to a stream using a simple 32bit RAW format.
        /// </summary>
        /// <param name="image">The image whose contents will be replaced with the loaded image data.</param>
        /// <param name="stream">The stream where the image data will be saved to.</param>
        /// <exception cref="ArgumentNullException">If the <see cref="image"/> or <see cref="stream"/> argument os null.</exception>
        /// <remarks>
        /// <p>The expected image format is described below.</p>
        /// <p>Pixel data is stored left to right, top to bottom.</p>
        /// <p>The last pixel does not end with a comma or crlf \r\n</p>
        /// <p>Width (4 bytes) followed by crlf \r\n<br />
        /// Height (4 bytes) followed by crlf \r\n<br />
        /// A,R,G,B,A,R,G,B ... etc A,R,G,B,\r\n<br />
        /// A,R,G,B,A,R,G,B ... etc A,R,G,B,\r\n<br />
        /// A,R,G,B,A,R,G,B ... etc A,R,G,B</p>
        /// </remarks>
        public static void Load32BppRaw(this GenericImage<Color> image, Stream stream)
        {
            if (image == null)
            {
                throw new ArgumentNullException("image");
            }

            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }

            var reader = new BinaryReader(stream);

            image.Width = reader.ReadInt32();
            var chars = reader.ReadChars(2);
            if (chars[0] != '\r' && chars[1] != '\n')
            {
                throw new BadImageFormatException("Expected carage return and line feed at end of width.");
            }

            image.Height = reader.ReadInt32();
            chars = reader.ReadChars(2);
            if (chars[0] != '\r' && chars[1] != '\n')
            {
                throw new BadImageFormatException("Expected carage return and line feed at end of height.");
            }

            var lastY = 0;
            image.ForEach(
                (genericImage, x, y) =>
                {
                    if (y > lastY)
                    {
                        lastY = y;
                        chars = reader.ReadChars(2);
                        if (chars[0] != '\r' && chars[1] != '\n')
                        {
                            throw new BadImageFormatException(string.Format("Expected carage return and line feed at end of line {0}.", y));
                        }
                    }

                    var a = reader.ReadByte();
                    if (reader.ReadChar() != ',')
                    {
                        throw new BadImageFormatException("Expected comma.");
                    }

                    var r = reader.ReadByte();
                    if (reader.ReadChar() != ',')
                    {
                        throw new BadImageFormatException("Expected comma.");
                    }
                    var g = reader.ReadByte();
                    if (reader.ReadChar() != ',')
                    {
                        throw new BadImageFormatException("Expected comma.");
                    }
                    var b = reader.ReadByte();

                    if (x == image.Width - 1 && y == image.Height - 1)
                    {
                        // last pixel so don't try to read comma 
                    }
                    else
                    {
                        if (reader.ReadChar() != ',')
                        {
                            throw new BadImageFormatException("Expected comma.");
                        }
                    }

                    genericImage[x, y] = new Color(r, g, b, a);
                });
        }
    }
}
