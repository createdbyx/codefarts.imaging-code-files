/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.Imaging
{
    /// <summary>
    /// The color.
    /// </summary>   
    public partial struct Color
    {
        #region KnownColors
        public static Color ActiveBorder
        {
            get
            {
                return new Color(180, 180, 180, 255);
            }
        }

        public static Color ActiveCaption
        {
            get
            {
                return new Color(153, 180, 209, 255);
            }
        }

        public static Color ActiveCaptionText
        {
            get
            {
                return new Color(0, 0, 0, 255);
            }
        }

        public static Color AppWorkspace
        {
            get
            {
                return new Color(171, 171, 171, 255);
            }
        }

        public static Color Control
        {
            get
            {
                return new Color(240, 240, 240, 255);
            }
        }

        public static Color ControlDark
        {
            get
            {
                return new Color(160, 160, 160, 255);
            }
        }

        public static Color ControlDarkDark
        {
            get
            {
                return new Color(105, 105, 105, 255);
            }
        }

        public static Color ControlLight
        {
            get
            {
                return new Color(227, 227, 227, 255);
            }
        }

        public static Color ControlLightLight
        {
            get
            {
                return new Color(255, 255, 255, 255);
            }
        }

        public static Color ControlText
        {
            get
            {
                return new Color(0, 0, 0, 255);
            }
        }

        public static Color Desktop
        {
            get
            {
                return new Color(0, 118, 163, 255);
            }
        }

        public static Color GrayText
        {
            get
            {
                return new Color(109, 109, 109, 255);
            }
        }

        public static Color Highlight
        {
            get
            {
                return new Color(51, 153, 255, 255);
            }
        }

        public static Color HighlightText
        {
            get
            {
                return new Color(255, 255, 255, 255);
            }
        }

        public static Color HotTrack
        {
            get
            {
                return new Color(0, 102, 204, 255);
            }
        }

        public static Color InactiveBorder
        {
            get
            {
                return new Color(244, 247, 252, 255);
            }
        }

        public static Color InactiveCaption
        {
            get
            {
                return new Color(191, 205, 219, 255);
            }
        }

        public static Color InactiveCaptionText
        {
            get
            {
                return new Color(0, 0, 0, 255);
            }
        }

        public static Color Info
        {
            get
            {
                return new Color(255, 255, 225, 255);
            }
        }

        public static Color InfoText
        {
            get
            {
                return new Color(0, 0, 0, 255);
            }
        }

        public static Color Menu
        {
            get
            {
                return new Color(240, 240, 240, 255);
            }
        }

        public static Color MenuText
        {
            get
            {
                return new Color(0, 0, 0, 255);
            }
        }

        public static Color ScrollBar
        {
            get
            {
                return new Color(200, 200, 200, 255);
            }
        }

        public static Color Window
        {
            get
            {
                return new Color(255, 255, 255, 255);
            }
        }

        public static Color WindowFrame
        {
            get
            {
                return new Color(100, 100, 100, 255);
            }
        }

        public static Color WindowText
        {
            get
            {
                return new Color(0, 0, 0, 255);
            }
        }

        public static Color Transparent
        {
            get
            {
                return new Color(255, 255, 255, 0);
            }
        }

        public static Color AliceBlue
        {
            get
            {
                return new Color(240, 248, 255, 255);
            }
        }

        public static Color AntiqueWhite
        {
            get
            {
                return new Color(250, 235, 215, 255);
            }
        }

        public static Color Aqua
        {
            get
            {
                return new Color(0, 255, 255, 255);
            }
        }

        public static Color Aquamarine
        {
            get
            {
                return new Color(127, 255, 212, 255);
            }
        }

        public static Color Azure
        {
            get
            {
                return new Color(240, 255, 255, 255);
            }
        }

        public static Color Beige
        {
            get
            {
                return new Color(245, 245, 220, 255);
            }
        }

        public static Color Bisque
        {
            get
            {
                return new Color(255, 228, 196, 255);
            }
        }

        public static Color Black
        {
            get
            {
                return new Color(0, 0, 0, 255);
            }
        }

        public static Color BlanchedAlmond
        {
            get
            {
                return new Color(255, 235, 205, 255);
            }
        }

        public static Color Blue
        {
            get
            {
                return new Color(0, 0, 255, 255);
            }
        }

        public static Color BlueViolet
        {
            get
            {
                return new Color(138, 43, 226, 255);
            }
        }

        public static Color Brown
        {
            get
            {
                return new Color(165, 42, 42, 255);
            }
        }

        public static Color BurlyWood
        {
            get
            {
                return new Color(222, 184, 135, 255);
            }
        }

        public static Color CadetBlue
        {
            get
            {
                return new Color(95, 158, 160, 255);
            }
        }

        public static Color Chartreuse
        {
            get
            {
                return new Color(127, 255, 0, 255);
            }
        }

        public static Color Chocolate
        {
            get
            {
                return new Color(210, 105, 30, 255);
            }
        }

        public static Color Coral
        {
            get
            {
                return new Color(255, 127, 80, 255);
            }
        }

        public static Color CornflowerBlue
        {
            get
            {
                return new Color(100, 149, 237, 255);
            }
        }

        public static Color Cornsilk
        {
            get
            {
                return new Color(255, 248, 220, 255);
            }
        }

        public static Color Crimson
        {
            get
            {
                return new Color(220, 20, 60, 255);
            }
        }

        public static Color Cyan
        {
            get
            {
                return new Color(0, 255, 255, 255);
            }
        }

        public static Color DarkBlue
        {
            get
            {
                return new Color(0, 0, 139, 255);
            }
        }

        public static Color DarkCyan
        {
            get
            {
                return new Color(0, 139, 139, 255);
            }
        }

        public static Color DarkGoldenrod
        {
            get
            {
                return new Color(184, 134, 11, 255);
            }
        }

        public static Color DarkGray
        {
            get
            {
                return new Color(169, 169, 169, 255);
            }
        }

        public static Color DarkGreen
        {
            get
            {
                return new Color(0, 100, 0, 255);
            }
        }

        public static Color DarkKhaki
        {
            get
            {
                return new Color(189, 183, 107, 255);
            }
        }

        public static Color DarkMagenta
        {
            get
            {
                return new Color(139, 0, 139, 255);
            }
        }

        public static Color DarkOliveGreen
        {
            get
            {
                return new Color(85, 107, 47, 255);
            }
        }

        public static Color DarkOrange
        {
            get
            {
                return new Color(255, 140, 0, 255);
            }
        }

        public static Color DarkOrchid
        {
            get
            {
                return new Color(153, 50, 204, 255);
            }
        }

        public static Color DarkRed
        {
            get
            {
                return new Color(139, 0, 0, 255);
            }
        }

        public static Color DarkSalmon
        {
            get
            {
                return new Color(233, 150, 122, 255);
            }
        }

        public static Color DarkSeaGreen
        {
            get
            {
                return new Color(143, 188, 139, 255);
            }
        }

        public static Color DarkSlateBlue
        {
            get
            {
                return new Color(72, 61, 139, 255);
            }
        }

        public static Color DarkSlateGray
        {
            get
            {
                return new Color(47, 79, 79, 255);
            }
        }

        public static Color DarkTurquoise
        {
            get
            {
                return new Color(0, 206, 209, 255);
            }
        }

        public static Color DarkViolet
        {
            get
            {
                return new Color(148, 0, 211, 255);
            }
        }

        public static Color DeepPink
        {
            get
            {
                return new Color(255, 20, 147, 255);
            }
        }

        public static Color DeepSkyBlue
        {
            get
            {
                return new Color(0, 191, 255, 255);
            }
        }

        public static Color DimGray
        {
            get
            {
                return new Color(105, 105, 105, 255);
            }
        }

        public static Color DodgerBlue
        {
            get
            {
                return new Color(30, 144, 255, 255);
            }
        }

        public static Color Firebrick
        {
            get
            {
                return new Color(178, 34, 34, 255);
            }
        }

        public static Color FloralWhite
        {
            get
            {
                return new Color(255, 250, 240, 255);
            }
        }

        public static Color ForestGreen
        {
            get
            {
                return new Color(34, 139, 34, 255);
            }
        }

        public static Color Fuchsia
        {
            get
            {
                return new Color(255, 0, 255, 255);
            }
        }

        public static Color Gainsboro
        {
            get
            {
                return new Color(220, 220, 220, 255);
            }
        }

        public static Color GhostWhite
        {
            get
            {
                return new Color(248, 248, 255, 255);
            }
        }

        public static Color Gold
        {
            get
            {
                return new Color(255, 215, 0, 255);
            }
        }

        public static Color Goldenrod
        {
            get
            {
                return new Color(218, 165, 32, 255);
            }
        }

        public static Color Gray
        {
            get
            {
                return new Color(128, 128, 128, 255);
            }
        }

        public static Color Green
        {
            get
            {
                return new Color(0, 128, 0, 255);
            }
        }

        public static Color GreenYellow
        {
            get
            {
                return new Color(173, 255, 47, 255);
            }
        }

        public static Color Honeydew
        {
            get
            {
                return new Color(240, 255, 240, 255);
            }
        }

        public static Color HotPink
        {
            get
            {
                return new Color(255, 105, 180, 255);
            }
        }

        public static Color IndianRed
        {
            get
            {
                return new Color(205, 92, 92, 255);
            }
        }

        public static Color Indigo
        {
            get
            {
                return new Color(75, 0, 130, 255);
            }
        }

        public static Color Ivory
        {
            get
            {
                return new Color(255, 255, 240, 255);
            }
        }

        public static Color Khaki
        {
            get
            {
                return new Color(240, 230, 140, 255);
            }
        }

        public static Color Lavender
        {
            get
            {
                return new Color(230, 230, 250, 255);
            }
        }

        public static Color LavenderBlush
        {
            get
            {
                return new Color(255, 240, 245, 255);
            }
        }

        public static Color LawnGreen
        {
            get
            {
                return new Color(124, 252, 0, 255);
            }
        }

        public static Color LemonChiffon
        {
            get
            {
                return new Color(255, 250, 205, 255);
            }
        }

        public static Color LightBlue
        {
            get
            {
                return new Color(173, 216, 230, 255);
            }
        }

        public static Color LightCoral
        {
            get
            {
                return new Color(240, 128, 128, 255);
            }
        }

        public static Color LightCyan
        {
            get
            {
                return new Color(224, 255, 255, 255);
            }
        }

        public static Color LightGoldenrodYellow
        {
            get
            {
                return new Color(250, 250, 210, 255);
            }
        }

        public static Color LightGray
        {
            get
            {
                return new Color(211, 211, 211, 255);
            }
        }

        public static Color LightGreen
        {
            get
            {
                return new Color(144, 238, 144, 255);
            }
        }

        public static Color LightPink
        {
            get
            {
                return new Color(255, 182, 193, 255);
            }
        }

        public static Color LightSalmon
        {
            get
            {
                return new Color(255, 160, 122, 255);
            }
        }

        public static Color LightSeaGreen
        {
            get
            {
                return new Color(32, 178, 170, 255);
            }
        }

        public static Color LightSkyBlue
        {
            get
            {
                return new Color(135, 206, 250, 255);
            }
        }

        public static Color LightSlateGray
        {
            get
            {
                return new Color(119, 136, 153, 255);
            }
        }

        public static Color LightSteelBlue
        {
            get
            {
                return new Color(176, 196, 222, 255);
            }
        }

        public static Color LightYellow
        {
            get
            {
                return new Color(255, 255, 224, 255);
            }
        }

        public static Color Lime
        {
            get
            {
                return new Color(0, 255, 0, 255);
            }
        }

        public static Color LimeGreen
        {
            get
            {
                return new Color(50, 205, 50, 255);
            }
        }

        public static Color Linen
        {
            get
            {
                return new Color(250, 240, 230, 255);
            }
        }

        public static Color Magenta
        {
            get
            {
                return new Color(255, 0, 255, 255);
            }
        }

        public static Color Maroon
        {
            get
            {
                return new Color(128, 0, 0, 255);
            }
        }

        public static Color MediumAquamarine
        {
            get
            {
                return new Color(102, 205, 170, 255);
            }
        }

        public static Color MediumBlue
        {
            get
            {
                return new Color(0, 0, 205, 255);
            }
        }

        public static Color MediumOrchid
        {
            get
            {
                return new Color(186, 85, 211, 255);
            }
        }

        public static Color MediumPurple
        {
            get
            {
                return new Color(147, 112, 219, 255);
            }
        }

        public static Color MediumSeaGreen
        {
            get
            {
                return new Color(60, 179, 113, 255);
            }
        }

        public static Color MediumSlateBlue
        {
            get
            {
                return new Color(123, 104, 238, 255);
            }
        }

        public static Color MediumSpringGreen
        {
            get
            {
                return new Color(0, 250, 154, 255);
            }
        }

        public static Color MediumTurquoise
        {
            get
            {
                return new Color(72, 209, 204, 255);
            }
        }

        public static Color MediumVioletRed
        {
            get
            {
                return new Color(199, 21, 133, 255);
            }
        }

        public static Color MidnightBlue
        {
            get
            {
                return new Color(25, 25, 112, 255);
            }
        }

        public static Color MintCream
        {
            get
            {
                return new Color(245, 255, 250, 255);
            }
        }

        public static Color MistyRose
        {
            get
            {
                return new Color(255, 228, 225, 255);
            }
        }

        public static Color Moccasin
        {
            get
            {
                return new Color(255, 228, 181, 255);
            }
        }

        public static Color NavajoWhite
        {
            get
            {
                return new Color(255, 222, 173, 255);
            }
        }

        public static Color Navy
        {
            get
            {
                return new Color(0, 0, 128, 255);
            }
        }

        public static Color OldLace
        {
            get
            {
                return new Color(253, 245, 230, 255);
            }
        }

        public static Color Olive
        {
            get
            {
                return new Color(128, 128, 0, 255);
            }
        }

        public static Color OliveDrab
        {
            get
            {
                return new Color(107, 142, 35, 255);
            }
        }

        public static Color Orange
        {
            get
            {
                return new Color(255, 165, 0, 255);
            }
        }

        public static Color OrangeRed
        {
            get
            {
                return new Color(255, 69, 0, 255);
            }
        }

        public static Color Orchid
        {
            get
            {
                return new Color(218, 112, 214, 255);
            }
        }

        public static Color PaleGoldenrod
        {
            get
            {
                return new Color(238, 232, 170, 255);
            }
        }

        public static Color PaleGreen
        {
            get
            {
                return new Color(152, 251, 152, 255);
            }
        }

        public static Color PaleTurquoise
        {
            get
            {
                return new Color(175, 238, 238, 255);
            }
        }

        public static Color PaleVioletRed
        {
            get
            {
                return new Color(219, 112, 147, 255);
            }
        }

        public static Color PapayaWhip
        {
            get
            {
                return new Color(255, 239, 213, 255);
            }
        }

        public static Color PeachPuff
        {
            get
            {
                return new Color(255, 218, 185, 255);
            }
        }

        public static Color Peru
        {
            get
            {
                return new Color(205, 133, 63, 255);
            }
        }

        public static Color Pink
        {
            get
            {
                return new Color(255, 192, 203, 255);
            }
        }

        public static Color Plum
        {
            get
            {
                return new Color(221, 160, 221, 255);
            }
        }

        public static Color PowderBlue
        {
            get
            {
                return new Color(176, 224, 230, 255);
            }
        }

        public static Color Purple
        {
            get
            {
                return new Color(128, 0, 128, 255);
            }
        }

        public static Color Red
        {
            get
            {
                return new Color(255, 0, 0, 255);
            }
        }

        public static Color RosyBrown
        {
            get
            {
                return new Color(188, 143, 143, 255);
            }
        }

        public static Color RoyalBlue
        {
            get
            {
                return new Color(65, 105, 225, 255);
            }
        }

        public static Color SaddleBrown
        {
            get
            {
                return new Color(139, 69, 19, 255);
            }
        }

        public static Color Salmon
        {
            get
            {
                return new Color(250, 128, 114, 255);
            }
        }

        public static Color SandyBrown
        {
            get
            {
                return new Color(244, 164, 96, 255);
            }
        }

        public static Color SeaGreen
        {
            get
            {
                return new Color(46, 139, 87, 255);
            }
        }

        public static Color SeaShell
        {
            get
            {
                return new Color(255, 245, 238, 255);
            }
        }

        public static Color Sienna
        {
            get
            {
                return new Color(160, 82, 45, 255);
            }
        }

        public static Color Silver
        {
            get
            {
                return new Color(192, 192, 192, 255);
            }
        }

        public static Color SkyBlue
        {
            get
            {
                return new Color(135, 206, 235, 255);
            }
        }

        public static Color SlateBlue
        {
            get
            {
                return new Color(106, 90, 205, 255);
            }
        }

        public static Color SlateGray
        {
            get
            {
                return new Color(112, 128, 144, 255);
            }
        }

        public static Color Snow
        {
            get
            {
                return new Color(255, 250, 250, 255);
            }
        }

        public static Color SpringGreen
        {
            get
            {
                return new Color(0, 255, 127, 255);
            }
        }

        public static Color SteelBlue
        {
            get
            {
                return new Color(70, 130, 180, 255);
            }
        }

        public static Color Tan
        {
            get
            {
                return new Color(210, 180, 140, 255);
            }
        }

        public static Color Teal
        {
            get
            {
                return new Color(0, 128, 128, 255);
            }
        }

        public static Color Thistle
        {
            get
            {
                return new Color(216, 191, 216, 255);
            }
        }

        public static Color Tomato
        {
            get
            {
                return new Color(255, 99, 71, 255);
            }
        }

        public static Color Turquoise
        {
            get
            {
                return new Color(64, 224, 208, 255);
            }
        }

        public static Color Violet
        {
            get
            {
                return new Color(238, 130, 238, 255);
            }
        }

        public static Color Wheat
        {
            get
            {
                return new Color(245, 222, 179, 255);
            }
        }

        public static Color White
        {
            get
            {
                return new Color(255, 255, 255, 255);
            }
        }

        public static Color WhiteSmoke
        {
            get
            {
                return new Color(245, 245, 245, 255);
            }
        }

        public static Color Yellow
        {
            get
            {
                return new Color(255, 255, 0, 255);
            }
        }

        public static Color YellowGreen
        {
            get
            {
                return new Color(154, 205, 50, 255);
            }
        }

        public static Color ButtonFace
        {
            get
            {
                return new Color(240, 240, 240, 255);
            }
        }

        public static Color ButtonHighlight
        {
            get
            {
                return new Color(255, 255, 255, 255);
            }
        }

        public static Color ButtonShadow
        {
            get
            {
                return new Color(160, 160, 160, 255);
            }
        }

        public static Color GradientActiveCaption
        {
            get
            {
                return new Color(185, 209, 234, 255);
            }
        }

        public static Color GradientInactiveCaption
        {
            get
            {
                return new Color(215, 228, 242, 255);
            }
        }

        public static Color MenuBar
        {
            get
            {
                return new Color(240, 240, 240, 255);
            }
        }

        public static Color MenuHighlight
        {
            get
            {
                return new Color(51, 153, 255, 255);
            }
        }
        #endregion
    }

    /// <summary>
    /// The color.
    /// </summary>   
    public partial struct Color
    {
        public bool Equals(Color other)
        {
            return this.A == other.A && this.B == other.B && this.G == other.G && this.R == other.R;
        }

        /// <summary>
        /// Indicates whether this instance and a specified object are equal.
        /// </summary>
        /// <returns>
        /// true if <paramref name="obj"/> and this instance are the same type and represent the same value; otherwise, false.
        /// </returns>
        /// <param name="obj">Another object to compare to. </param>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            return obj is Color && this.Equals((Color)obj);
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer that is the hash code for this instance.
        /// </returns>
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = this.A.GetHashCode();
                hashCode = (hashCode * 397) ^ this.B.GetHashCode();
                hashCode = (hashCode * 397) ^ this.G.GetHashCode();
                hashCode = (hashCode * 397) ^ this.R.GetHashCode();
                return hashCode;
            }
        }

        #region Constants and Fields

        /// <summary>
        /// Alpha component.
        /// </summary>
        public byte A;

        /// <summary>
        /// Blue component.
        /// </summary>
        public byte B;

        /// <summary>
        /// Green component.
        /// </summary>
        public byte G;

        /// <summary>
        /// Red component.
        /// </summary>
        public byte R;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Color"/> struct. 
        /// Initializes a new instance of the<see cref="Color"/>struct.
        /// </summary>
        /// <param name="r">
        /// Red component.
        /// </param>
        /// <param name="g">
        /// Green component.
        /// </param>
        /// <param name="b">
        /// Blue component.
        /// </param>
        /// <param name="a">
        /// Alpha component.
        /// </param>
        public Color(byte r, byte g, byte b, byte a)
        {
            this.R = r;
            this.G = g;
            this.B = b;
            this.A = a;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Returns a new color from the specified color components.
        /// </summary>
        /// <param name="a">
        /// The alpha value.
        /// </param>
        /// <param name="r">
        /// The red value.
        /// </param>
        /// <param name="g">
        /// The green value.
        /// </param>
        /// <param name="b">
        /// The blue value.
        /// </param>
        /// <returns>
        /// Return the new color.
        /// </returns>
        public static Color FromArgb(byte a, byte r, byte g, byte b)
        {
            return new Color(r, g, b, a);
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The color components as a string.
        /// </summary>
        /// <returns>
        /// Returns a string containing the color components.
        /// </returns>
        public override string ToString()
        {
            return string.Format("A:{0} R:{1} G:{2} B:{3}", this.A, this.R, this.G, this.B);
        }

        /// <summary>
        /// Compares two colors for equality.
        /// </summary>
        /// <param name="left">
        /// The Point on the left side of the sign.
        /// </param>
        /// <param name="right">
        /// The Point on the right side of the sign.
        /// </param>
        /// <returns>
        /// Returns a new <see cref="Point"/>.
        /// </returns>
        public static bool operator ==(Color left, Color right)
        {
            return (left.A == right.A && left.R == right.R && left.B == right.B && left.G == right.G);
        }

        /// <summary>
        /// Compares two colors for inequality.
        /// </summary>
        /// <param name="left">
        /// The Point on the left side of the sign.
        /// </param>
        /// <param name="right">
        /// The Point on the right side of the sign.
        /// </param>
        /// <returns>
        /// Returns a new <see cref="Point"/>.
        /// </returns>
        public static bool operator !=(Color left, Color right)
        {
            return !(left == right);
        }

        #endregion
    }
}