﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.Imaging
{
    using System;

    /// <summary>
    /// Extension methods for the <see cref="Color"/> type.
    /// </summary>
    public static class ColorExtensionMethods
    {
        #region Public Methods and Operators

        /// <summary>
        /// Blends two colors together.
        /// </summary>
        /// <param name="sourceColor">
        /// The source color.
        /// </param>
        /// <param name="blendColor">
        /// The blend color.
        /// </param>
        /// <returns>
        /// Returns the result as a <see cref="Color"/> type.
        /// </returns>
        /// <remarks>
        /// From Wikipedia https://en.wikipedia.org/wiki/Alpha_compositing -> "Alpha blending" 
        /// </remarks>
        public static Color Blend(this Color sourceColor, Color blendColor)
        {
            var sr = blendColor.R / 255f;
            var sg = blendColor.G / 255f;
            var sb = blendColor.B / 255f;
            var sa = blendColor.A / 255f;
            var dr = sourceColor.R / 255f;
            var dg = sourceColor.G / 255f;
            var db = sourceColor.B / 255f;
            var da = sourceColor.A / 255f;

            var oa = sa + (da * (1 - sa));
            var r = ((sr * sa) + ((dr * da) * (1 - sa))) / oa;
            var g = ((sg * sa) + ((dg * da) * (1 - sa))) / oa;
            var b = ((sb * sa) + ((db * da) * (1 - sa))) / oa;
            var a = oa;

            // if alpha is 0 just return source color else build new blended color
            return Math.Abs(a - 0) < Constants.Epsilon ? sourceColor : new Color((byte)(r * 255), (byte)(g * 255), (byte)(b * 255), (byte)(a * 255));
        }

        /// <summary>
        /// Packs color components into a <see cref="uint"/> type.
        /// </summary>
        /// <param name="color">The color to pack.</param>
         /// <returns>Returns a <see cref="uint"/> representing the color.</returns>
        public static uint Pack(this Color color)
        {
            var r = color.R;
            var g = color.G;
            var b = color.B;
            var a = color.A;

            return ((uint)r & 0xff) | (((uint)g & 0xff) << 8) | (((uint)b & 0xff) << 16) | (((uint)a & 0xff) << 24);
        }

        /// <summary>
        /// Unpacks a <see cref="uint"/> type into a color type.
        /// </summary>
        /// <param name="value">The packed color value to unpack.</param>
        /// <returns>Returns a color type from the unpacked values.</returns>
        public static Color UnpackColor(this uint value)
        {
            var r = (byte)value;
            var g = (byte)(value >> 8);
            var b = (byte)(value >> 16);
            var a = (byte)(value >> 24);

            return new Color(r, g, b, a);
        }

        /// <summary>
        /// Packs color components into a <see cref="uint"/> type.
        /// </summary>
        /// <param name="r">The red component.</param>
        /// <param name="g">The green component.</param>
        /// <param name="b">The blue component.</param>
        /// <param name="a">The alpha component.</param>
        /// <returns>Returns a <see cref="uint"/> representing the color.</returns>
        public static uint Pack(int r, int g, int b, int a)
        {
            return ((uint)r & 0xff) | (((uint)g & 0xff) << 8) | (((uint)b & 0xff) << 16) | (((uint)a & 0xff) << 24);
        }

        /// <summary>
        /// Unpacks a <see cref="uint"/> type into a color type.
        /// </summary>
        /// <param name="value">The packed color value to unpack.</param>
        /// <returns>Returns a color type from the unpacked values.</returns>
        public static Color Unpack(uint value)
        {
            var r = (byte)value;
            var g = (byte)(value >> 8);
            var b = (byte)(value >> 16);
            var a = (byte)(value >> 24);

            return new Color(r, g, b, a);
        }

        #endregion
    }
}