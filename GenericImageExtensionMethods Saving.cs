﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
namespace Codefarts.Imaging
{
    using System;
    using System.IO;
    using System.Linq;

    /// <summary>
    /// Contains extension methods for the <see cref="GenericImage{T}"/> type.
    /// </summary>
    public static partial class GenericImageExtensionMethods
    {
        public static void SaveToPPM(this GenericImage<Color> bitmap, Stream stream)
        {
            // Use a streamwriter to write the text part of the encoding
            var writer = new StreamWriter(stream);
            writer.Write("P6" + " ");
            writer.Write(bitmap.Width + " " + bitmap.Height + " ");
            writer.Write("255" + " ");
            writer.Flush();

            // Switch to a binary writer to write the data
            var writerB = new BinaryWriter(stream);
            writerB.Seek(0, SeekOrigin.End);
            for (var x = 0; x < bitmap.Height; x++)
            {
                for (var y = 0; y < bitmap.Width; y++)
                {
                    var color = bitmap[y, x];
                    writerB.Write(color.R);
                    writerB.Write(color.G);
                    writerB.Write(color.B);
                }
            }

            writerB.Flush();
        }


        /// <summary>
        /// Saves a <see cref="GenericImage{T}"/> to a stream using a simple 32 bit RAW format.
        /// </summary>
        /// <param name="image">The image to be saved.</param>
        /// <param name="stream">The stream where the image data will be saved to.</param>
        /// <exception cref="ArgumentNullException">If the <see cref="image"/> or <see cref="stream"/> argument is null.</exception>
        /// <remarks>
        /// <p>The image format is described below.</p>
        /// <p>Pixel data is stored left to right, top to bottom.</p>
        /// <p>The last pixel does not end with a comma or crlf \r\n</p>
        /// <p>Width (4 bytes) followed by crlf \r\n<br />
        /// Height (4 bytes) followed by crlf \r\n<br />
        /// Palette count (4 bytes) followed by crlf \r\n<br />
        /// Palette entry (4 bytes) packed color followed by crlf \r\n<br />
        /// ... more palette entries ...
        /// Palette Index,Palette Index,Palette Index,Palette Index,Palette Index, ... etc \r\n<br />
        /// Palette Index,Palette Index,Palette Index,Palette Index,Palette Index, ... etc \r\n<br />
        /// Palette Index,Palette Index,Palette Index,Palette Index,Palette Index, ... etc </p>
        /// </remarks>
        public static void SavePalatalized(this GenericImage<Color> image, Stream stream)
        {
            if (image == null)
            {
                throw new ArgumentNullException("image");
            }

            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }

            var colorList = image.PixelGrid.Distinct().ToList();

            var writer = new BinaryWriter(stream);
            writer.Write(image.Width);
            writer.Write('\r');
            writer.Write('\n');
            writer.Write(image.Height);
            writer.Write('\r');
            writer.Write('\n');
            writer.Write(colorList.Count);
            writer.Write('\r');
            writer.Write('\n');
            var lastY = 0;

            // write palette data
            foreach (var color in colorList)
            {
                writer.Write(color.Pack());
                writer.Write('\r');
                writer.Write('\n');
            }

            // write palette index data for each pixel
            image.ForEach(
                (genericImage, x, y) =>
                {
                    if (y > lastY)
                    {
                        lastY = y;
                        writer.Write('\r');
                        writer.Write('\n');
                    }

                    var color = colorList.IndexOf(genericImage[x, y]);

                    writer.Write(color);
                    writer.Write(',');
                    if (x == image.Width - 1 && y == image.Height - 1)
                    {
                        // last pixel so don't write comma 
                    }
                    else
                    {
                        writer.Write(',');
                    }
                });

            writer.Flush();
        }

        /// <summary>
        /// Saves a <see cref="GenericImage{T}"/> to a stream using a simple 32 bit RAW format.
        /// </summary>
        /// <param name="image">The image to be saved.</param>
        /// <param name="stream">The stream where the image data will be saved to.</param>
        /// <exception cref="ArgumentNullException">If the <see cref="image"/> or <see cref="stream"/> argument is null.</exception>
        /// <remarks>
        /// <p>The image format is described below.</p>
        /// <p>Pixel data is stored left to right, top to bottom.</p>
        /// <p>The last pixel does not end with a comma or crlf \r\n</p>
        /// <p>Width (4 bytes) followed by crlf \r\n<br />
        /// Height (4 bytes) followed by crlf \r\n<br />
        /// A,R,G,B,A,R,G,B ... etc A,R,G,B,\r\n<br />
        /// A,R,G,B,A,R,G,B ... etc A,R,G,B,\r\n<br />
        /// A,R,G,B,A,R,G,B ... etc A,R,G,B</p>
        /// </remarks>
        public static void Save32BppRaw(this GenericImage<Color> image, Stream stream)
        {
            if (image == null)
            {
                throw new ArgumentNullException("image");
            }

            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }

            var writer = new BinaryWriter(stream);
            writer.Write(image.Width);
            writer.Write('\r');
            writer.Write('\n');
            writer.Write(image.Height);
            writer.Write('\r');
            writer.Write('\n');
            var lastY = 0;
            image.ForEach(
                (genericImage, x, y) =>
                {
                    if (y > lastY)
                    {
                        lastY = y;
                        writer.Write('\r');
                        writer.Write('\n');
                    }

                    var color = genericImage[x, y];

                    writer.Write(color.A);
                    writer.Write(',');
                    writer.Write(color.R);
                    writer.Write(',');
                    writer.Write(color.G);
                    writer.Write(',');
                    writer.Write(color.B);
                    if (x == image.Width - 1 && y == image.Height - 1)
                    {
                        // last pixel so don't write comma 
                    }
                    else
                    {
                        writer.Write(',');
                    }
                });

            writer.Flush();
        }
    }
}
